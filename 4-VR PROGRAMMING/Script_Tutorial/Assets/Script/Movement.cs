﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
     private float maximumDamage = 120 ;
     public float minimumDamage = 45f;
    
     public float flashIntensity = 3f;
     public float fadeSpeed = 10f;

     private Animator anim;
     private Light laserShotLight;
     private SphereCollider col;
    public AudioClip shotClip;



    public GameObject[] players;




    public float speed=2;
    // Start is
    // called before the first frame update
    private void Awake()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        Transform tr = GetComponent<Transform>();
    }

    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        
             for (int i = 0; i < players.Length; i++)
             {
             Debug.Log(" Player Number " + i + " is named " + players[i].name);
             }


       
    }
    private void OnEnable()
    {
        Debug.Log(" OnEnable ");
    }

    private void OnDisable()
    {
        Debug.Log(" OnDisable ");
    }
    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(h, 0, v);
        gameObject.transform.Translate(direction.normalized * Time.deltaTime * speed);
    }
    private void FixedUpdate()
    {
        Debug.Log(" FixedUpdate ");
    }
    private void OnTriggerEnter(Collider other)
    {
        speed = speed * -1;
        Debug.Log(speed);
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("OnTriggerExit");
    }
    private void OnTriggerStay(Collider other)
    {
        Debug.Log("OnTriggerStay");
    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("OnCollisionEnter");
    }
    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("OnCollisionExit");
    }
    private void OnCollisionStay(Collision collision)
    {
        Debug.Log("OnCollisionExit");
    }
    private void OnGUI()
    {
        
    }
    private void OnMouseDown()
    {
        
    }
    private void OnMouseEnter()
    {
        
    }
    private void OnMouseUp()
    {
        
    }
    private void OnDestroy()
    {
        
    }

    


}
